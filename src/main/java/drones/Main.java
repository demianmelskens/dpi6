package drones;

import domain.Drone;
import domain.Vector3;
import util.interfaces.DroneFrame;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

public class Main extends JFrame implements DroneFrame {

    private static final long serialVersionUID = 1L;
    private static final int nrOfDrones = 3;

    private JPanel contentPane;
    private DefaultListModel<String> listModel = new DefaultListModel<String>();
    private JList<String> list;

    private Drone[] drones = new Drone[nrOfDrones];
    private Collection<Thread> threads = new ArrayList<Thread>();

    public static void main(String [] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Main frame = new Main();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public Main() {
        setTitle("Drones");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[]{46, 31, 86, 30, 89, 0};
        gbl_contentPane.rowHeights = new int[]{233, 23, 0};
        gbl_contentPane.columnWeights = new double[]{1.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
        contentPane.setLayout(gbl_contentPane);
        JScrollPane scrollPane = new JScrollPane();
        GridBagConstraints gbc_scrollPane = new GridBagConstraints();
        gbc_scrollPane.gridwidth = 7;
        gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
        gbc_scrollPane.fill = GridBagConstraints.BOTH;
        gbc_scrollPane.gridx = 0;
        gbc_scrollPane.gridy = 0;
        contentPane.add(scrollPane, gbc_scrollPane);

        list = new JList<>(listModel);
        scrollPane.setViewportView(list);

        createDrones();
        launchDrones();
    }

    private void createDrones(){
        Random _random = new Random(1);
        for(int i = 0; i < drones.length; ++i){
            drones[i] = new Drone(i, Vector3.randomVector(100), _random, this);
        }
    }

    private void launchDrones(){
        for (Drone d: drones) {
            System.out.println(d.toString());
            Thread thread = new Thread(d);
            thread.start();
            threads.add(thread);
        }
    }

    @Override
    public void add(String info) {
        this.listModel.addElement(info);
    }
}
