package drones.connection;

import domain.Drone;
import domain.DroneMessage;
import util.interfaces.DroneFrame;
import util.jms.JmsReceiver;

import javax.jms.Message;
import javax.jms.ObjectMessage;

public class Receiver extends JmsReceiver {

    private DroneFrame frame;
    private Drone drone;

    public Receiver(String queue, DroneFrame frame, Drone drone){
        super();
        initConnection(queue);
        this.frame = frame;
        this.drone = drone;
    }

    @Override
    public void onMessage(Message message) {
        try{
            ObjectMessage msg=(ObjectMessage)message;
            DroneMessage drone = (DroneMessage) msg.getObject();
            this.frame.add(drone.toString());
            this.drone.processOtherDrone(drone);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
