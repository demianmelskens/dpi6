package util.interfaces;

import domain.Drone;
import domain.DroneMessage;

import java.util.List;

public interface BrokerFrame {
    List<DroneMessage> getDrones();
    void add(String info);
    void addDrone(DroneMessage drone);
    void addSender(DroneMessage drone);
    void sendPosition(DroneMessage drone);
}
