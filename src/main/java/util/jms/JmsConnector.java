package util.jms;

import org.apache.activemq.ActiveMQConnectionFactory;
import javax.jms.*;

public class JmsConnector {

    private static  JmsConnector inst;
    private Session session;
    private Connection conn;
    private String queue;
    private Destination destination;

    private MessageConsumer consumer;
    private MessageProducer producer;


    public static JmsConnector getInstance(String queue)
    {
        if (inst == null)
        {
            inst = new JmsConnector(queue);
        }
        return  inst;
    }


    public JmsConnector(String queue) {
        try
        {
            this.queue = queue;
            ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://88.159.248.116:61616");
            conn = connectionFactory.createConnection();
            conn.start();

            //Creating a non transactional session to send/receive JMS message.
            session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);

            //Destination represents here our queue 'JCG_QUEUE' on the JMS server.
            //The queue will be created automatically on the server.
            destination = session.createQueue(queue);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public boolean isConnectionOpen()
    {
        return true;
    }

    public MessageProducer getSender() {
        try
        {
            producer = session.createProducer(destination);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return producer;
    }

    public Session getSes() {
        return session;
    }

    public MessageConsumer getReceiver() {

        try
        {
            if (consumer == null) {
                consumer = session.createConsumer(destination);
                return consumer;
            }
            return consumer;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
}