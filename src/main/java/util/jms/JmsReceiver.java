package util.jms;

import domain.Drone;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

public class JmsReceiver implements MessageListener {

    private JmsConnector conn;
    private Drone drone;

    public JmsReceiver() {
    }

    public void initConnection(String queue) {
        try {
            conn = new JmsConnector(queue);
            conn.getReceiver().setMessageListener(this);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMessage(Message message) {
        try{
            ObjectMessage msg=(ObjectMessage)message;
            System.out.println("following message is received:"+msg.toString());
        }catch(Exception e){
            e.printStackTrace();
        }
    }


}