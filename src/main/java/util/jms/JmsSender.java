package util.jms;

import domain.Drone;

import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import java.io.Serializable;

public class JmsSender {

    private JmsConnector conn;

    public JmsSender() {
    }

    public void initConnection(String queue) {
        this.conn = new JmsConnector(queue);
    }

    public void sendMessage(Serializable information) {
        try {
            MessageProducer sender = conn.getSender();
            ObjectMessage message = conn.getSes().createObjectMessage();
            message.setObject(information);

            sender.send(message);
            sender.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}