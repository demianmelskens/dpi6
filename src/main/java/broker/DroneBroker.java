package broker;

import broker.connection.Receiver;
import broker.connection.Sender;
import domain.Drone;
import domain.DroneMessage;
import util.interfaces.BrokerFrame;
import util.jms.JmsReceiver;
import util.jms.JmsSender;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DroneBroker extends JFrame implements BrokerFrame {
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private DefaultListModel<String> listModel = new DefaultListModel<String>();
    private JList<String> list;

    private List<DroneMessage> drones;
    private JmsReceiver receiver;
    private Map<Long, JmsSender> senders;

    /**
     * starts the application
     * @param args
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    DroneBroker frame = new DroneBroker();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public DroneBroker() {
        setTitle("Drone Broker");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[]{46, 31, 86, 30, 89, 0};
        gbl_contentPane.rowHeights = new int[]{233, 23, 0};
        gbl_contentPane.columnWeights = new double[]{1.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
        contentPane.setLayout(gbl_contentPane);
        JScrollPane scrollPane = new JScrollPane();
        GridBagConstraints gbc_scrollPane = new GridBagConstraints();
        gbc_scrollPane.gridwidth = 7;
        gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
        gbc_scrollPane.fill = GridBagConstraints.BOTH;
        gbc_scrollPane.gridx = 0;
        gbc_scrollPane.gridy = 0;
        contentPane.add(scrollPane, gbc_scrollPane);

        list = new JList<>(listModel);
        scrollPane.setViewportView(list);

        this.drones = new ArrayList<DroneMessage>();
        this.senders = new HashMap<Long, JmsSender>();
        this.receiver = new Receiver(this);
    }

    @Override
    public List<DroneMessage> getDrones(){
        return this.drones;
    }

    @Override
    public void add(String info) {
        this.listModel.addElement(info);
    }

    @Override
    public void addDrone(DroneMessage drone) {
        this.drones.add(drone);
    }

    @Override
    public void addSender(DroneMessage drone){
        this.senders.put(drone.getId(), new Sender("drone_" + drone.getId()));
    }

    @Override
    public void sendPosition(DroneMessage drone){
        for(DroneMessage d : this.drones){
            if(d.getPos().distance(drone.getPos()) < 2){
                this.senders.get(d.getId()).sendMessage(drone);
            }
        }
    }
}
