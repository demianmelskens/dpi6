package broker.connection;

import domain.Drone;
import domain.DroneMessage;
import util.interfaces.BrokerFrame;
import util.jms.JmsReceiver;

import javax.jms.Message;
import javax.jms.ObjectMessage;

public class Receiver extends JmsReceiver {

    private BrokerFrame frame;

    public Receiver(BrokerFrame frame){
        super();
        initConnection("drone");
        this.frame = frame;
    }

    @Override
    public void onMessage(Message message) {
        try{
            ObjectMessage msg=(ObjectMessage)message;
            DroneMessage drone = (DroneMessage) msg.getObject();
            isDroneKnown(drone);
            frame.sendPosition(drone);
            frame.add("Recieved position: " + drone.toString());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void isDroneKnown(DroneMessage drone){
        if (!frame.getDrones().contains(drone)){
            frame.addDrone(drone);
            frame.addSender(drone);
        }
    }
}
