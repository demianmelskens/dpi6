package domain;

import drones.connection.Receiver;
import drones.connection.Sender;
import util.interfaces.DroneFrame;
import util.jms.JmsReceiver;
import util.jms.JmsSender;

import java.io.Serializable;
import java.util.Random;

public class Drone implements Runnable, Serializable {
    private boolean crashed = false, landed = false;
    private long id, movementSpeed = 2;
    private Vector3 pos;
    private Random _random;

    private JmsSender sender;
    private JmsReceiver receiver;

    /**
     * zero args contstructor
     */
    public Drone() {
    }

    /**
     * constructor
     * @param id
     * @param pos
     */
    public Drone(long id, Vector3 pos, Random random, DroneFrame frame) {
        this.id = id;
        this.pos = pos;
        this._random = random;
        this.sender = new Sender();
        this.receiver = new Receiver("drone_" + this.id, frame, this);
    }

    /**
     * main run method
     */
    public void run() {
        while(!crashed){
            try {
                sender.sendMessage(new DroneMessage(this));
                move(new Vector3(_random.nextLong(), _random.nextLong(), _random.nextLong()));
                Thread.sleep(1000);
            }catch (InterruptedException ex){
                ex.printStackTrace();
            }
        }
        Thread.currentThread().interrupt();
    }

    /**
     * adds a force to the position to make the drone move.
     * @param vec
     */
    public void move(Vector3 vec){
        this.pos.addForce(vec);
    }

    public void processOtherDrone(DroneMessage drone){
        if (this.id != drone.getId()){
            move(drone.getPos().opposite());
        }
    }

    public boolean isCrashed() {
        return crashed;
    }

    public void setCrashed(boolean crashed) {
        this.crashed = crashed;
    }

    public boolean isLanded() {
        return landed;
    }

    public void setLanded(boolean landed) {
        this.landed = landed;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMovementSpeed() {
        return movementSpeed;
    }

    public void setMovementSpeed(long movementSpeed) {
        this.movementSpeed = movementSpeed;
    }

    public Vector3 getPos() {
        return pos;
    }

    public void setPos(Vector3 pos) {
        this.pos = pos;
    }

    @Override
    public String toString() {
        return "Drone{" +
                "crashed=" + crashed +
                ", landed=" + landed +
                ", id=" + id +
                ", movementSpeed=" + movementSpeed +
                ", pos=" + pos +
                '}';
    }
}
