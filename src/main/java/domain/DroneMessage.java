package domain;

import java.io.Serializable;

public class DroneMessage implements Serializable {
    private boolean crashed = false, landed = false;
    private long id, movementSpeed = 2;
    private Vector3 pos;

    public DroneMessage(){

    }

    public DroneMessage(Drone drone){
        this.crashed = drone.isCrashed();
        this.landed = drone.isLanded();
        this.id = drone.getId();
        this.movementSpeed = drone.getMovementSpeed();
        this.pos = drone.getPos();
    }

    public DroneMessage(boolean crashed, boolean landed, long id, long movementSpeed, Vector3 pos) {
        this.crashed = crashed;
        this.landed = landed;
        this.id = id;
        this.movementSpeed = movementSpeed;
        this.pos = pos;
    }

    public boolean isCrashed() {
        return crashed;
    }

    public void setCrashed(boolean crashed) {
        this.crashed = crashed;
    }

    public boolean isLanded() {
        return landed;
    }

    public void setLanded(boolean landed) {
        this.landed = landed;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMovementSpeed() {
        return movementSpeed;
    }

    public void setMovementSpeed(long movementSpeed) {
        this.movementSpeed = movementSpeed;
    }

    public Vector3 getPos() {
        return pos;
    }

    public void setPos(Vector3 pos) {
        this.pos = pos;
    }

    @Override
    public String toString() {
        return "DroneMessage{" +
                "crashed=" + crashed +
                ", landed=" + landed +
                ", id=" + id +
                ", movementSpeed=" + movementSpeed +
                ", pos=" + pos +
                '}';
    }
}
