package domain;

import java.io.Serializable;
import java.util.Random;

public class Vector3 implements Serializable {
    public long xPos, yPos, zPos;

    /**
     * zero args constructor
     */
    public Vector3() {
    }

    /**
     * constructor for vector3
     * @param xPos
     * @param yPos
     * @param zPos
     */
    public Vector3(long xPos, long yPos, long zPos) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.zPos = zPos;
    }

    /**
     * adds a force to existing vector
     * @param vec
     * @return Vector3
     */
    public Vector3 addForce(Vector3 vec){
        this.xPos += vec.xPos;
        this.yPos += vec.yPos;
        this.zPos += vec.zPos;
        return this;
    }

    /**
     * returns the distance from this vector to given position.
     * @param pos
     * @return float
     */
    public float distance (Vector3 pos) {
        final float a = pos.xPos - this.xPos;
        final float b = pos.yPos - this.yPos;
        final float c = pos.zPos - this.zPos;
        return (float)Math.sqrt(a * a + b * b + c * c);
    }

    /**
     * returns the opposite vector from this vector.
     * @return Vector3
     */
    public Vector3 opposite(){
        return new Vector3(-this.xPos, -this.yPos, -this.zPos);
    }

    /**
     * returns a random vector 3 object within the max value
     * @param maxVal
     * @return Vector3
     */
    public static Vector3 randomVector(int maxVal){
        Random _random = new Random();
        return new Vector3(_random.nextInt(maxVal), _random.nextInt(maxVal), _random.nextInt(maxVal));
    }

    @Override
    public String toString() {
        return "xPos=" + xPos +
                ", yPos=" + yPos +
                ", zPos=" + zPos;
    }
}
